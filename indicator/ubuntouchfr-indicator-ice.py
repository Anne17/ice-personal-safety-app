import sys
import os
import json
import urllib.request
import subprocess
import shlex
import logging

from gi.repository import Gio
from gi.repository import GLib

BUS_NAME = 'com.ubuntouchfr.indicator.ice'
BUS_OBJECT_PATH = '/com/ubuntouchfr/indicator/ice'
BUS_OBJECT_PATH_PHONE = BUS_OBJECT_PATH + '/phone'

logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


class iceIndicator(object):
    ROOT_ACTION = 'root'
    MAIN_SECTION = 0

    config_file = "/home/phablet/.config/indicator-ice.ubuntouchfr/config.json"  # TODO don't hardcode this
    doctornum = ''    
    doctorname = ''    
    infopersonnal = ''

    def __init__(self, bus):
        self.get_config()

        self.bus = bus
        self.action_group = Gio.SimpleActionGroup()
        self.menu = Gio.Menu()
        self.sub_menu = Gio.Menu()

        self.current_switch_icon = "unlike"

    def get_config(self):
        with open(self.config_file, 'r') as f:
            config_json = {}
            try:
                config_json = json.load(f)
            except:
                logger.warning('Failed to load the config file: {}'.format(str(sys.exc_info()[1])))

            if 'doctornum' in config_json:
                self.doctornum = config_json['doctornum'].strip()

            if 'doctorname' in config_json:
                self.doctorname = config_json['doctorname'].strip()

            if 'infopersonnal' in config_json:
                self.infopersonnal = config_json['infopersonnal'].strip()



    def get_text(self, condition):
        text = 'Indicator ICE'
        return text

    def get_icon(self, condition):
        icon = self.FOG
        return icon

    def _setup_actions(self):
        root_action = Gio.SimpleAction.new_stateful(self.ROOT_ACTION, None, self.root_state())
        self.action_group.insert(root_action)

    def _create_section(self):
        section = Gio.Menu()

        personnal_menu_item = Gio.MenuItem.new('Personnal information', 'indicator.{}')
        icon = Gio.ThemedIcon.new_with_default_fallbacks('contact')
        personnal_menu_item.set_attribute_value('icon', icon.serialize())
        section.append_item(personnal_menu_item)

        for str in self.infopersonnal.split('\n'):
            personnalinfo_menu_item = Gio.MenuItem.new(str, 'indicator.{}')
            section.append_item(personnalinfo_menu_item)            
        

        medical_menu_item = Gio.MenuItem.new('Medical information', 'indicator.{}')
        icon = Gio.ThemedIcon.new_with_default_fallbacks('find')
        medical_menu_item.set_attribute_value('icon', icon.serialize())
        section.append_item(medical_menu_item)
        
        for str in self.doctorname.split('\n'):
            doctorname_menu_item = Gio.MenuItem.new(str, 'indicator.{}')
            section.append_item(doctorname_menu_item)           



        contact_menu_item = Gio.MenuItem.new('Contact information', 'indicator.{}')
        icon = Gio.ThemedIcon.new_with_default_fallbacks('call-start')
        contact_menu_item.set_attribute_value('icon', icon.serialize())
        section.append_item(contact_menu_item)
        
        for str in self.doctornum.split('\n'):
            doctornum_menu_item = Gio.MenuItem.new(str, 'indicator.{}')
            section.append_item(doctornum_menu_item)

        return section

    def _setup_menu(self):
        self.sub_menu.insert_section(self.MAIN_SECTION, 'ice', self._create_section())

        root_menu_item = Gio.MenuItem.new('ice', 'indicator.{}'.format(self.ROOT_ACTION))
        root_menu_item.set_attribute_value('x-canonical-type', GLib.Variant.new_string('com.canonical.indicator.root'))
        root_menu_item.set_submenu(self.sub_menu)
        self.menu.append_item(root_menu_item)

    def _update_menu(self):
        self.sub_menu.remove(self.MAIN_SECTION)
        self.sub_menu.insert_section(self.MAIN_SECTION, 'ice', self._create_section())

    def run(self):
        self._setup_actions()
        self._setup_menu()

        self.bus.export_action_group(BUS_OBJECT_PATH, self.action_group)
        self.menu_export = self.bus.export_menu_model(BUS_OBJECT_PATH_PHONE, self.menu)

    def root_state(self):
        vardict = GLib.VariantDict.new()
        vardict.insert_value('visible', GLib.Variant.new_boolean(True))
        vardict.insert_value('title', GLib.Variant.new_string('ICE'))

        icon = Gio.ThemedIcon.new(self.current_icon())
        vardict.insert_value('icon', icon.serialize())

        return vardict.end()

    def current_icon(self):
        icon = self.current_switch_icon
        return icon


if __name__ == '__main__':
    bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)
    proxy = Gio.DBusProxy.new_sync(bus, 0, None, 'org.freedesktop.DBus', '/org/freedesktop/DBus', 'org.freedesktop.DBus', None)
    result = proxy.RequestName('(su)', BUS_NAME, 0x4)
    if result != 1:
        logger.critical('Error: Bus name is already taken')
        sys.exit(1)

    wi = iceIndicator(bus)
    wi.run()

    logger.debug('ice Indicator startup completed')
    GLib.MainLoop().run()