#!/bin/bash

set -e

rm /home/phablet/.config/upstart/ubuntouchfr-indicator-ice.conf
rm /home/phablet/.local/share/unity/indicators/com.ubuntouchfr.indicator.ice

echo "indicator-ice uninstalled"
