#!/bin/bash

set -e

mkdir -p /home/phablet/.config/upstart/
mkdir -p /home/phablet/.local/share/unity/indicators/

cp -v /opt/click.ubuntu.com/indicator-ice.ubuntouchfr/current/indicator/ubuntouchfr-indicator-ice.conf /home/phablet/.config/upstart/
cp -v /opt/click.ubuntu.com/indicator-ice.ubuntouchfr/current/indicator/com.ubuntouchfr.indicator.ice /home/phablet/.local/share/unity/indicators/

echo "indicator-ice installed!"
